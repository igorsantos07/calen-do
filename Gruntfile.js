/*global module:false*/
module.exports = function(grunt) {

	// Project configuration.
	grunt.initConfig({
		// Metadata.
		pkg: grunt.file.readJSON('package.json'),

		// Task configuration.
		uglify: {
			options: {
				banner: '<%= banner %>'
			},
			dist: {
				src: 'app/application.js',
				dest: 'dist/application.js'
			}
		},

		less: {
			dev: {
				files: { 'app/css/styles.css': 'app/less/styles.less' },
			},
			prod: {
				files: { 'app/css/styles.css': 'app/less/styles.less' },
				options: { yuicompress: true }
			}
		},

		htmlmin: {
			dist: {
				files: {'dist/index.html': 'app/index.html'},
				options: {
					removeComments: true,
					collapseWhitespace: true
				}
			}
		},

		imagemin: {
			dist: {
				options: {
					optimizationLevel: 7,
					progressive: true
				},
				files: {
					'dist/imgs/': 'app/imgs/*',
					'dist/imgs/icons/': 'app/imgs/icons/*',
					'dist/css/images/': 'app/css/jquery-ui/cupertino/images/*'
				}
			}
		},

		cssmin: {
			dist: {
				files: {
					'dist/css/styles.css': [
						'app/css/jquery-ui/cupertino/jquery-ui.min.css',
						'app/css/styles.css'
					]
				},
				options: {
					keepSpecialComments: 0,
					report: 'min'
				}
			}
		},

		clean: ['dist'],

		jasmine: {
			test: {
				src: 'app/application.js',
				options: {
					specs: 'app/tests/*.js',
					styles: 'css/styles.css'
				}
			}
		},

		watch: {
			js: {
				files: [
					'app/controllers/**',
					'app/less/**',
					'app/extras/**',
					'app/less/**',
					'app/models/**',
					'app/routes/**',
					'app/templates/**',
					'app/vendor/**',
					'app/views/**',
					'routes.js',
					'store.js',
					'app.js'
				],
				tasks: ['build']
			},
			less: {
				files: ['app/less/*'],
				tasks: ['less:dev']
			}
		}
	});

	// These plugins provide necessary tasks.
	(function() {
		grunt.loadNpmTasks('grunt-contrib-concat');
		grunt.loadNpmTasks('grunt-contrib-uglify');
		grunt.loadNpmTasks('grunt-contrib-htmlmin');
		grunt.loadNpmTasks('grunt-contrib-imagemin');
		grunt.loadNpmTasks('grunt-contrib-cssmin');
		grunt.loadNpmTasks('grunt-contrib-less');
		grunt.loadNpmTasks('grunt-contrib-jasmine');
		grunt.loadNpmTasks('grunt-contrib-clean');
		grunt.loadNpmTasks('grunt-contrib-watch');
	})()

	grunt.registerTask('build', 'Runs ember-tools build command', function() {
		var done = this.async();
		grunt.task.run('less:dev');
		grunt.util.spawn({ cmd: 'ember', args: ['build'] }, function(e, result) {
			grunt.log.writeln(result);
			done();
		});
	});

	grunt.registerTask('prepare-dist', 'Creates folders needed for distribution', function() {
		var task = this;
		grunt.util._.each(['dist/css/images', 'dist/imgs/icons'], function(folder) {
			var done = task.async();
			grunt.util.spawn({ cmd: 'mkdir', args: ['-p', folder] }, function(e, result) {
				grunt.log.writeln('Folder '+folder+' created');
				done();
			});
		});
	});

	grunt.registerTask('autobuild', 'Builds and builds again when changes happen.', function() {
		grunt.task.run('build')
		grunt.task.run('watch')
	});

	grunt.registerTask('default', ['dist']);

	grunt.registerTask('dist', [
		'clean',
		'prepare-dist',
		'build',
		'compress',
		'jasmine'
	]);

	grunt.registerTask('compress', [
		'less:prod',
		'htmlmin',
		'imagemin',
		'cssmin',
		'uglify'
	]);

	grunt.registerTask('css', ['less:dev']);

	grunt.registerTask('css-prod', ['less:prod']);

};
