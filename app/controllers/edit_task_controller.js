var EditTaskController = Ember.ObjectController.extend({

  save: function() {
	var model = this.get('model')

	model.set('isDelegated', !!model.get('delegatedTo'))

    this.get('store').commit();
    this.redirectToModel();
  },

  redirectToModel: function() {
    var router = this.get('target');
    var model = this.get('model');
    router.transitionTo('tasks_folder', 'now');
  }

});

module.exports = EditTaskController;

