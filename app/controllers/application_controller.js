var ApplicationController = Ember.Controller.extend({

	user: {
		name: {
			first: 'Igor',
			last: 'Santos'
		}
	},
	date: (new Date()).toLocaleString(),
	tasks: {
		length: 10,
		plural: function() {
			return this.get('tasks').length > 1
		}
	},

	menuItem: Em.View.extend({
		tagName: 'li',
		previousColor: '',

		dragEnter: function (ev) {
			var $target = $(ev.target)
			this.previousColor = $target.css('backgroundColor')
			console.log(this.previousColor)
			$target.css('backgroundColor', '#CCC')
		},

		dragLeave: function (ev) {
			$(ev.target).css('backgroundColor', this.previousColor)
		},

		click: function (ev) {
			var $button = $(ev.target),
				text = $button.html().trim().toLowerCase()

			this.get('controller').transitionToRoute('tasks_folder', Ember.A([text]))
		}
	})

});

module.exports = ApplicationController;