var TasksNowView = Ember.View.extend({

	didInsertElement: function() {
		//TODO: this should be changed into an appropriate action: http://emberjs.com/guides/templates/actions/

		$("ul#left")
            .on("dragstart", "li", function(ev) {
                ev.dataTransfer.setData("task-id", $(this).attr("data-id"));
            });

		$(".collapsable-list .section").click(function() {
			var $icon = $(this).children('span');
			if ($icon.hasClass('icon-down')) {
				$icon.removeClass('icon-down');
				$icon.addClass('icon-right');
			} else {
				$icon.removeClass('icon-right');
				$icon.addClass('icon-down');
			}

			var list_item = this;
			var list = this.parentElement;
			var next = false;
			$(list).children().each(function () {
				if(next) {
					if (! $(this).is(':visible')) {
						$( this ).show( 'blind', {}, 500, null );
					}
					else {
						$( this ).hide( 'blind', {}, 500, null );
					}
					next = false;
				}
				else if(this == list_item) {
					next = true;
				}
			});
		});

		$(".collapsable-list .section-options li").click(function() {
			var $li = $(this);
			if($li.hasClass("sel")) {
				$li.removeClass("sel");
			}
			else {
				$li.addClass("sel");
			}
		});
	}
})

module.exports = TasksNowView