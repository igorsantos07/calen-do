Task = require('../models/task.js');
var ApplicationView = Ember.View.extend({

	didInsertElement: function() {
		//TODO: turn all this into real ember actions

		$("#trash")
			.on("dragenter",function() {
				$(this).addClass("sel");
			})
			.on("dragleave",function() {
				$(this).removeClass("sel");
			})
			.on("dragover", function(ev) {
				ev.preventDefault();
			})
			.on("drop", function(ev) {
                var taskId = ev.dataTransfer.getData("task-id");
                var task = Task.find(taskId);
                task.deleteRecord();
                task.get('store').commit();
                $("ul#left li[data-id='" + taskId + "']").remove();
				$(this).removeClass("sel");
			});

		$("#buttons li")
			.on("dragenter", function(ev) {
				$(this).addClass("sel");
			})
			.on("dragleave", function(ev) {
				$(this).removeClass("sel");
			})
			.on("dragover", function(ev) {
				ev.preventDefault();
			})
			.on("drop", function(ev) {
				console.log("Task-id: " + ev.dataTransfer.getData("task-id"));
                var taskId = ev.dataTransfer.getData("task-id");
                var task = Task.find(taskId);
                task.set('isDone','true');
                task.get('store').commit();
                $("ul#left li[data-id='" + taskId + "']").remove();
				$(this).removeClass("sel");
			});

        $("#list>div li").click(function() {
            console.log("Open task " + $(this).text());
        });

        $("#date-icon-down").click(function() {
            var d = getFirstDate();
            d.setDate(d.getDate() + 7);
            setFirstDate(d);
        });

        $("#date-icon-up").click(function() {
            var d = getFirstDate();
            d.setDate(d.getDate() - 7);
            setFirstDate(d);
        });

        var dateI = new Date(Date.now());
        $("#day li")
            .each(function() {
                $(this).attr("value", dateI.getUTCFullYear() + "-" + (dateI.getUTCMonth() + 1) + "-" + dateI.getUTCDate());
                $(this).text(formatDate(dateI));
                dateI.setDate(dateI.getDate() + 1);
            })
            .click(function() {
                if(!$(this).hasClass("sel")) {
                    $("#day li.sel").removeClass("sel");
                    $(this).addClass("sel");
                    var text = $(this).text();
                    var cod = $(this).attr("value");
                    $("#list>div h2").text(text);
                    console.log("Open tasks from " + text + "\n" + cod);
                }
            });

        function getFirstDate() {
            return new Date($("#day li:first").attr("value"));
        }

        function setFirstDate(date) {
            if(!date instanceof Date)
                throw new error("setFirstDate(d): parameter d is not an instance of Date.");
            var fDate = getFirstDate();
            var dropDir;
            if (fDate < date) dropDir = "up";
            else dropDir = "down";

            $("#day li").each(function() {
                $(this).attr("value", date.getUTCFullYear() + "-" + (date.getUTCMonth() + 1) + "-" + date.getUTCDate());
                date.setDate(date.getDate() + 1);
            });
            $("#day ul").hide("drop", {direction: dropDir}, "normal", function() {
                $("#day li").each(function() {
                    var day = new Date($(this).attr("value"));
                    $(this).text(formatDate(day));
                    if($(this).text() === $("#list>div h2").text()) $(this).addClass("sel");
                    else if($(this).hasClass("sel")) $(this).removeClass("sel");
                });
                $(this).show("drop", {direction: (dropDir === "down"? "up": "down")}, "normal");
            });
        }

        function formatDate(date) {
            if(!date instanceof Date)
                throw new error("formatDate(d): parameter d is not an instance of Date.");

            var dateStr;
            var now = new Date(Date.now());
            if (now.getUTCFullYear() === date.getUTCFullYear() &&
                    now.getUTCMonth() === date.getUTCMonth()) {
                if (now.getUTCDate() === date.getUTCDate()) return "Today";
                now.setDate(now.getDate() + 1);
                if (now.getUTCDate() === date.getUTCDate()) return "Tomorrow";
            }
            switch(date.getUTCDay()) {
                case 0: dateStr = "Sun"; break;
                case 1: dateStr = "Mon"; break;
                case 2: dateStr = "Tue"; break;
                case 3: dateStr = "Wed"; break;
                case 4: dateStr = "Thu"; break;
                case 5: dateStr = "Fri"; break;
                case 6: dateStr = "Sat"; break;
            }
            dateStr += " ";
            switch(date.getUTCMonth()) {
                case 0: dateStr += "Jan"; break;
                case 1: dateStr += "Feb"; break;
                case 2: dateStr += "Mar"; break;
                case 3: dateStr += "Apr"; break;
                case 4: dateStr += "May"; break;
                case 5: dateStr += "Jun"; break;
                case 6: dateStr += "Jul"; break;
                case 7: dateStr += "Aug"; break;
                case 8: dateStr += "Sep"; break;
                case 9: dateStr += "Oct"; break;
                case 10: dateStr += "Nov"; break;
                case 11: dateStr += "Dec"; break;
            }
            dateStr += " " + date.getUTCDate();
            return dateStr;
        }
	}

})

module.exports = ApplicationView