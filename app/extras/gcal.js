/*
 * TODO: this should be verified; code was migrated, but not sure it's working. Now we have an object/function
 * but I'm not sure those nested functions will be accessible when this module is required. Probably the best
 * way to fix this would be using a simple object with properties, such as:
 * return {
 *		clientId: 'xxxx',
 *		pageLoad: function() {
 *			window.setTimeOut(this.checkAuth, 1)
 *		},
 * }
 *
 * A cleaner way would be using a function() {} referencing this.<<property>> inside it, but if we ever change
 * this to some kind of Ember object, the conversion will be easier with the other approach.
 */
require(['googleAPI'], function() {
	return function() {
		/* Enter a client ID for a web application from the Google Developer Console. In your Developer Console project, add a JavaScript origin that corresponds to the domain where you will be running the script*/
		var clientId = '976279442880.apps.googleusercontent.com';
		/*Enter the API key from the Google Develoepr Console - to handle any unauthenticated requests in the code.*/
		var apiKey = 'AIzaSyD0ylBAGZ6heeAYbXnq5PVPul5OqWlkxj8';
		/*To enter one or more authentication scopes, refer to the documentation for the API.*/
		var scopes = ['https://www.googleapis.com/auth/plus.me',
			'https://www.googleapis.com/auth/calendar'];


		//All the authentication stuff
		function pageLoad() {
			gapi.client.setApiKey(apiKey);
			window.setTimeout(checkAuth, 1);
		}
		function checkAuth() {
			gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: true}, handleAuthResult);
		}
		function handleAuthResult(authResult) {
			var authorizeButton = document.getElementById('authorize-button');
			if (authResult && !authResult.error) {
				authorizeButton.style.visibility = 'hidden';
				authorizeButton.value = 'Authenticated!';
				getName();
			} else {
				authorizeButton.style.visibility = '';
				authorizeButton.value = "Auth Problem";
				authorizeButton.onclick = handleAuthClick;
			}
		}
		function handleAuthClick(event) {
			gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: false}, handleAuthResult);
			return false;
		}
		/*After authenticated this method is called as a call back to get the Google + users name*/
		function getName() { //RPC method
			gapi.client.load('plus', 'v1', function() {
				var request = gapi.client.plus.people.get({
					'userId': 'me'
				});
				request.execute(function(resp) {
					var heading = document.createElement('h4');
					var image = document.createElement('img');
					image.src = resp.image.url;
					heading.appendChild(image);
					heading.appendChild(document.createTextNode(resp.displayName));

					document.getElementById('user').appendChild(heading);
					RESTgetCalendars();
				});
			});
		}

		var cal = 'primary';
		function RESTgetCalendars() {
			gapi.client.load('calendar', 'v3', function() {
				var request = gapi.client.request({
					'path': '/calendar/v3/users/me/calendarList',
					'method': 'GET',
					'minAccessRole': 'owner'
				});
				request.execute(function(resp) {
					var item = document.createElement('P');
					for (var i = 0; i < resp.items.length; i++) {
						item.appendChild(document.createTextNode(resp.items[i].summary + ' ' + resp.items[i].id));
						var button = document.createElement('BUTTON');
						button.innerHTML = "getEvents";
						button.id = 'cal-' + resp.items[i].id;
						button.onclick = function() {
							cal = this.id.substr(4);
							RESTgetEvents();
						};
						item.appendChild(button);
						item.appendChild(document.createElement('br'));
					}
					document.getElementById('cals').innerHTML = "";
					document.getElementById('cals').appendChild(item);
				});
			});
		}
		function RESTgetEvents() {
			gapi.client.load('calendar', 'v3', function() {
				var request = gapi.client.request({
					'path': '/calendar/v3/calendars/' + cal + '/events',
					'method': 'GET',
					'calendarId': cal
				});
				request.execute(function(resp) {
					var item = document.createElement('P');
					var i;
					for (i = 0; i < resp.items.length; i++) {
						item.appendChild(document.createTextNode(resp.items[i].summary + ' ' + resp.items[i].start.dateTime + ' ' + resp.items[i].id));
						var button = document.createElement('BUTTON');
						button.innerHTML = "delete";
						button.id = 'event-' + resp.items[i].id;
						button.onclick = function() {
							RESTdeleteEvent(this.id.substr(6));
						};
						item.appendChild(button);
						item.appendChild(document.createElement('br'));
					}
					document.getElementById('events').innerHTML = "";
					document.getElementById('events').appendChild(item);
				});
			});
		}
		function RESTdeleteEvent(val) {
			gapi.client.load('calendar', 'v3', function() {
				var request = gapi.client.request({
					'path': '/calendar/v3/calendars/' + cal + '/events/' + val,
					'method': 'DELETE',
					'calendarId': cal,
					'eventId': val
				});
				request.execute(function(resp) {
					var item = document.createElement('P');
					item.appendChild(document.createTextNode(val));
					document.getElementById('events').appendChild(item);
				});
				RESTgetEvents(); //load eventlist again to prove event was deleted
			});
		}
		function RESTcreateEvent(val) {
			gapi.client.load('calendar', 'v3', function() {
				var newcontent = {
					"summary": val,
					"start": {
						"dateTime": "2013-03-27T10:00:00.000-07:00"
					},
					"end": {
						"dateTime": "2013-03-27T12:25:00.000-07:00"
					}
				};
				var request = gapi.client.request({
					'path': '/calendar/v3/calendars/' + cal + '/events',
					'method': 'POST',
					'body': newcontent
				});
				request.execute(function(resp) {
					document.getElementById('newevent').value = '';
					RESTgetEvents(); //load eventlist again to prove event was deleted
				});
			});
		}
	}
})