var Proj = require('../models/proj');

var ProjRoute = Ember.Route.extend({

  model: function() {
    return Proj.find();
  }

});

module.exports = ProjRoute;

