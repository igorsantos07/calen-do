var task = require('../models/task');

var NewTaskRoute = Ember.Route.extend({

  renderTemplate: function() {
    this.render('edit_task', {controller: 'new_task'});
  },

  model: function() {
    return task.createRecord();
  },

  exit: function() {
    var model = this.get('controller.model');
    if (!model.get('isSaving')) {
      model.deleteRecord();
    }
  }

});

module.exports = NewTaskRoute;

