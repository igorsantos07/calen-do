var proj = require('../models/proj');

var NewProjRoute = Ember.Route.extend({

  renderTemplate: function() {
    this.render('edit_proj', {controller: 'new_proj'});
  },

  model: function() {
    return proj.createRecord();
  },

  exit: function() {
    var model = this.get('controller.model');
    if (!model.get('isSaving')) {
      model.deleteRecord();
    }
  }

});

module.exports = NewProjRoute;

