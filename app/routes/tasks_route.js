var Task = require('../models/task');

var TaskRoute = Ember.Route.extend({

  model: function() {
    return Task.find();
  }

});

module.exports = TaskRoute;

