var Task = require('../models/task'),
	_ = require('../vendor/underscore.js');


var TasksNowRoute = Ember.Route.extend({
	modelCalled: false,
	folder: '',

	serialize: function(folder) {
		return { folder: folder }
	},

	model: function(model) {
		var folder, _model = _(model)
		if (_model.isString())
			folder = model
		else if (_model.isArray())
			folder = model[0]
		else
			folder = model.folder

		this.modelCalled = true
		return folder
	},

	setupController: function(controller, folder) {
		if (!this.modelCalled)
			folder = this.model(folder)

		this.modelCalled = false

		var tasks = [],
			title = 'Next Actions'

		switch (folder) {
			case 'now':
				tasks = Task.find({ isDone: false });
			break;

			case 'done':
				title = 'Done tasks'
				tasks = Task.find({ isDone: true });
			break;

			case 'waiting':
				title = 'Tasks delegated to someone'
				tasks = Task.find({ isDelegated: true });
			break;

			case 'active':
				//what's this??
				tasks = Task.find({ isDone: false });
			break;

			case 'someday':
				title = 'Future tasks / ideas'
				tasks = Task.find({ isSomeday: true });
			break;

			case 'references':
				title = 'References for other tasks'
				tasks = Task.find({ isReference: true });
			break;
		}

//		console.log(tasks.get('length'))
//		tasks = _(tasks).first(10)
//		console.log(tasks.length)

		tasks.then(function(allTasks) {
			controller.set('content', {
				title: title,
				folder: folder,
				tasks: allTasks
			})
		})
	}
});

module.exports = TasksNowRoute;