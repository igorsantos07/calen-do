var Task = DS.Model.extend({
	title: DS.attr('string'),
	description: DS.attr('string'),
	priority: DS.attr('number'),
	due: DS.attr('date'),
	delegatedTo: DS.attr('string'),
	isDelegated: DS.attr('boolean'),
	isDone: DS.attr('boolean'),
	isSomeday: DS.attr('boolean'),
	isReference: DS.attr('boolean'),

	availablePriorities: [
		{num: 1, value: 'Very High'},
		{num: 2, value: 'High'},
		{num: 3, value: 'Normal'},
		{num: 4, value: 'Low'},
		{num: 5, value: 'Very Low'}
	],

	priorityClass: function() {
		return 'p'+this.get('priority')
	}.property('priority')

});

module.exports = Task;